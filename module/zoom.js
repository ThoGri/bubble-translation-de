class SimpleZoom { static async Init(controls, html) {

        const diceRollbtn = $(
            `
            <li class="scene-control zoom-scene-control" data-control="simple-zoom" title="Simple Zoom powered by Thomas">
                <i class="far fa-circle"></i>
            
            </li>
            `
        );

        html.append(diceRollbtn);

        diceRollbtn[0].addEventListener('click', ev => this.PopupSheet(ev, html));

    }
 
  static async PopupSheet(event, html) {
      let scene =  game.scenes.get(canvas.scene.data._id);
      let blub = scene.data.grid;
      console.log('blub', blub);
      let zoomScale = (113 / scene.data.grid);
      console.log('zoomScale', zoomScale);
        canvas.pan({ scale: zoomScale }); 
    }
}

Hooks.on('renderSceneControls', (controls, html) => { SimpleZoom.Init(controls, html); });
Hooks.on('init', () => {
 game.settings.register("bubble-DE", "szScale", {
        name: "Scale Factor",
        hint: "Ei Scale halt",
        scope: "world",
        type: Number,
        default: 0.85,
        config: true,
        onChange: s => { 
            console.log('simple Zoom onChangeProperty')
         }
    });
})
console.log("SimpleZoome | Simple Zoom loaded");