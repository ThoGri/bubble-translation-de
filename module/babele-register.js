Hooks.once('init', () => {

	if(typeof Babele !== 'undefined') {
		
		Babele.get().register({
			module: 'bubble-translation-de',
			lang: 'de',
			dir: 'compendium'
		});
	}
});
